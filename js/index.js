"use strict"

/*

1-
цей метод відміняє дефолтну поведінку. типу:
           -Клік на посилання ініціює навігацію до його URL-адреси.
           -Клік на кнопку відправки форми ініціює її відправку на сервер.
           -Натискання кнопки миші на тексті і переміщення курсору – виділяє текст.

2-
сенс у тому, щоб не ставити окремо прослуховувачі на багато елементів, з їх спільним предком.
ідея полягає у тому щоб поставити один прослуховувач на їх спільний батьківський елемент

3-
основні події (якщо я правильно зрозумів питання). це
DOMContentLoaded – браузер повністю завантажив HTML, створено дерево DOM, ресурси, такі як зображення і css, скоріше, ще не завантажені.
load – завантажено не тільки HTML, а й усі зображення, стилі тощо.
beforeunload/unload – користувач покидає сторінку.

-------------------------------------------------------------------------------------------------------------------------------------
*/

//1-
const contentButton = document.querySelector(".tabs");
const txt = document.querySelectorAll(".tabs-content");
contentButton.addEventListener("click", (event) => {
    event.stopPropagation();
    const classActive = document.querySelector(".active");
    const revision = document.querySelector("#revision")
    if (classActive) {
        classActive.classList.remove("active");
    }
    if (revision) {
        revision.removeAttribute("id");
    }
    event.target.classList.add("active");

    txt.forEach(elem => {
        const li = elem.querySelector(`.${event.target.innerText}`);
        li.id = "revision"
        console.log(li)
    })
});